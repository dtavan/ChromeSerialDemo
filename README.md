ChromeSerialDemo
================

A Chrome App that shows how to communicate with an Arduino using the Serial API. This example is largely inspired by @renaun [MyChromeArduinoBlink app](http://github.com/renaun/ArduinoExamples/tree/master/MyChromeArduinoBlink). I kept the basic idea and look & feel but rewrote most of the code to make it work with the new Serial API. 

This app displays a slider that, when dragged, changes the duration of the blink on the arduino led (pin 13 on Arduino board). The Arduino also sends back the blink count, the led state and the blink duration. This way you can figure out how data is sent to and received from the Arduino board using the Chrome Serial API. The interface also shows the console output to make it easier to track data sent and received. To mimic what the Arduino is doing on pin 13, the Arduino logo turns on and off at the same pace as the led on pin 13.

1. Install the sketch on your Arduino board.
2. Install and launch the chrome app.
3. Select serial port from dropdown list and click connect.
4. Move slider to change blink duration.

## Install Arduino Sketch

Upload sketch found in the `arduino_sketch` folder of this repository on your arduino board.

## Install Chrome App

You can clone this repository and load the unpacked extension, or you can also directly install it from the Chrome Web Store using the link below.

<a target="_blank" href="https://chrome.google.com/webstore/detail/dclblpkjodlbjcgjkoigjfdcchacnalj">![Try it now in CWS](https://raw.github.com/dtavan/ChromeSerialDemo/master/assets/tryitnowbutton.png "Click here to install this sample from the Chrome Web Store")</a>

## Future version 

I am currently working on adapting this demo to use the Firmata protocol to control the Arduino board using only JS code.

## APIs

* [Chrome Serial API](https://developer.chrome.com/apps/serial)
     
## Screenshot
![screenshot](https://raw.github.com/dtavan/ChromeSerialDemo/master/assets/screenshot_1280_800.png)



