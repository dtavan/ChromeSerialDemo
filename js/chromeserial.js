/*

  Chrome Serial API Demo
  Basic script that shows how to manage serial connection between a Chrome App and an Arduino.
 
  Copyright (C) 2014 Damien Tavan @dtavan http://damientavan.com
  
  This content is released under the MIT License (http://opensource.org/licenses/MIT).

*/

var connectionId;
var portPath;
var portPicker = document.querySelector('#port-picker');
var connectBtn = document.querySelector('#btn_connect');
var disconnectBtn = document.querySelector('#btn_disconnect');
var positionSlider = document.querySelector('#position-input');
var clearconsoleBtn = document.querySelector('#btn_clearconsole');
var blinkLength = 1000; // Default to 1000 milliseconds (1 second) for blink length

// Convert ArrayBuffer to string
var ab2str = function(buf) {
  var bufView = new Uint8Array(buf);
  return String.fromCharCode.apply(null, bufView);
};

// Convert string to ArrayBuffer
var str2ab = function(str) {
  var buf = new ArrayBuffer(str.length);
  var bufView = new Uint8Array(buf);
  for (var i = 0; i < str.length; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
};

// Sets the status message and color depending on connection state
var setStatus = function(status, msg) {
  if (status == 1) {
    document.querySelector('#status').className = 'connected';
  } 
  else {
    document.querySelector('#status').className = 'disconnected';
  }
  document.querySelector('#status').innerText = msg;
}

// Writes log output to console-output section
function log(msg) {
  var buffer = document.querySelector('#console-output');
  buffer.innerHTML += '> ' + msg + '<br/>';
  buffer.scrollTop = buffer.scrollHeight;
}

// As a result of calling getDevices, build a list of available ports to be displayed
var buildPortPicker = function(ports) {
  ports.forEach(function(port) {
    var portOption = document.createElement('option');
    portOption.value = portOption.innerText = port.path;
    portPicker.appendChild(portOption);
  });
};

// Get a list of serial ports
chrome.serial.getDevices(buildPortPicker);

// Called upon sending data on serial connection
var onSend = function(sendInfo) {
  console.log('Nbr. of Bytes Sent: ' + sendInfo.bytesSent); // Output to Javascript console
  log('Nbr. of Bytes Sent: ' + sendInfo.bytesSent); // Output to console-output section of interface
}

// Send message over serial connection
var sendMsg = function(val) {
  var buffer = str2ab(val);
  console.log('Msg Sent over Serial: ' + val); // Output to Javascript console
  log('Msg Sent over Serial: ' + val); // Output to console-output section of interface
  chrome.serial.send(connectionId, buffer, onSend);
};

// Called upon receiving data once serial connection is established
var onReceiveCallback = function(receiveInfo) {
  if (receiveInfo.connectionId == connectionId && receiveInfo.data) {
    var value = ab2str(receiveInfo.data);
    var m = /([^:]+):([-]?\d+)/.exec(value);
    //console.log('Received: ' + value);
    //console.log('Regexp: ' + m[1] + ' ' + m[2]);
    // Light on and off
    if (m[1] == 'a') { 
      console.log('Received CMD[a]: ' + m[2]); // Output to Javascript console
      log('Received CMD[a]: ' + m[2]); // Output to console-output section of interface
      var opat = isNaN(parseInt(m[2])) ? 0 : parseInt(m[2]);
      document.querySelector('#image').style.opacity = (opat* 0.7) + 0.3;
    }
    // Return blink length value
    else if (m[1] == "b") {
      console.log('Received CMD[b]: ' + m[2]); // Output to Javascript console
      log('Received CMD[b]: ' + m[2]); // Output to console-output section of interface
    }
    // Blink Count
    else if (m[1] == 'c') { 
      console.log('Received CMD[c]: ' + m[2]); // Output to Javascript console
      log('Received CMD[c]: ' + m[2]); // Output to console-output section of interface
      document.querySelector('#blinkCount').innerText = m[2];
    }
  }
};

// As a result of trying to connect to serial port, give feedback on connection status
var onConnect = function(connectionInfo) {
  if (!connectionInfo) {
    setStatus(0, 'Connection failed');
    return;
  }
  // The serial port has been opened. Save its id to use later.
  connectionId = connectionInfo.connectionId;
  // This is how we capture data received on serial port with the new API
  chrome.serial.onReceive.addListener(onReceiveCallback);
  // Output connection status.
  setStatus(1, 'Connected to ' + portPath);
  // Send blink length value to arduino
  sendMsg(blinkLength.toString(16));
};

connectBtn.onclick = function() {
  // Obtain selected value from dropdown list
  portPath = portPicker.options[portPicker.selectedIndex].value;
  // Connect to the selected serial port
  chrome.serial.connect(portPath, { bitrate: 57600 }, onConnect);
};

var onDisconnect = function(result) {
  if (result) {
    setStatus(0, 'Disconnected');
  } else {
    setStatus(1, 'Disconnect failed');
  }
};

disconnectBtn.onclick = function() {
  // Disconnect the current serial connection
  chrome.serial.disconnect(connectionId, onDisconnect);
}

positionSlider.onchange = function() {
 blinkLength = parseInt(this.value, 10);
 var blinkLengthTxt = document.querySelector('#blink-length');
 if (blinkLength <= 10) {
   blinkLengthTxt.innerText = this.value/1000 + ' second';
 }
 else {
   blinkLengthTxt.innerText = this.value/1000 + ' seconds';
 }
 sendMsg(blinkLength.toString(16));
};

clearconsoleBtn.onclick = function() {
  var buffer = document.querySelector('#console-output');
  buffer.innerHTML = '';
  buffer.scrollTop = buffer.scrollHeight;
};

