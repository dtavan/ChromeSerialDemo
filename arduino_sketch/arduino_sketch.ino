/*

  Chrome Serial API Demo Sketch
  Turns led on and off for length passed in milliseconds over serial connection.
 
  Copyright (C) 2014 Damien Tavan @dtavan http://damientavan.com
  
  This content is released under the MIT License (http://opensource.org/licenses/MIT).

*/
 
#define BLINK_LED 13

int ledState = LOW;
int lightOnLength = 1000; // Blink length in milliseconds 
int blinkCount = 0;
long count_timer = 0;

// Converting from Hex to Decimal:
// NOTE: This function can handle a positive hex value from 0 - 65,535 (a four digit hex string).
//       For larger/longer values, change "unsigned int" to "long" in both places.
// Source: https://github.com/benrugg/Arduino-Hex-Decimal-Conversion
unsigned int hexToDec(String hexString) {  
  unsigned int decValue = 0;
  int nextInt;
  for (int i = 0; i < hexString.length(); i++) {
    nextInt = int(hexString.charAt(i));
    if (nextInt >= 48 && nextInt <= 57) nextInt = map(nextInt, 48, 57, 0, 9);
    if (nextInt >= 65 && nextInt <= 70) nextInt = map(nextInt, 65, 70, 10, 15);
    if (nextInt >= 97 && nextInt <= 102) nextInt = map(nextInt, 97, 102, 10, 15);
    nextInt = constrain(nextInt, 0, 15);
    decValue = (decValue * 16) + nextInt;
  }
  return decValue;
}

// Converting from Decimal to Hex:
// NOTE: This function can handle a positive decimal value from 0 - 255, and it will pad it
//       with 0's (on the left) if it is less than the desired string length.
//       For larger/longer values, change "byte" to "unsigned int" or "long" for the decValue parameter.
// Source: https://github.com/benrugg/Arduino-Hex-Decimal-Conversion
String decToHex(byte decValue, byte desiredStringLength) {
  String hexString = String(decValue, HEX);
  while (hexString.length() < desiredStringLength) hexString = "0" + hexString;
  return hexString;
}

void setup() {
  Serial.begin(57600);  
  // initialize the digital pin as an output.
  pinMode(BLINK_LED, OUTPUT);
}

void switchLedState() {
  digitalWrite(BLINK_LED, ledState);
  delay(lightOnLength);
  Serial.print("a:"); Serial.println(ledState);
  delay(10); // Make time between sends to limit buffer accumulation
}

void getSerialData() {
  String inputString = ""; // a string to hold incoming data
  // if there's any serial available, read it:
  while (Serial.available() > 0) {
    // get the new byte:
    char inChar = Serial.read(); 
    // add it to the inputString:
    inputString += inChar;
  }
  if (inputString.length() > 0) {
    lightOnLength = hexToDec(inputString);
    Serial.print("b:"); Serial.println(lightOnLength);
    delay(10);
  }
}

void loop() {
  
  // Get data received from serial connection and pass it onto local variables
  getSerialData();
  
  ledState = HIGH;
  switchLedState();

  ledState = LOW;
  switchLedState();
    
  Serial.print("c:"); Serial.println(blinkCount++);
  delay(10);

}

